// Handler for .ready() called.

// parseUri 1.2.2
// (c) Steven Levithan <stevenlevithan.com>
// MIT License

function parseUri (str) {
	var	o   = parseUri.options,
		m   = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
		uri = {},
		i   = 14;

	while (i--) uri[o.key[i]] = m[i] || "";

	uri[o.q.name] = {};
	uri[o.key[12]].replace(o.q.parser, function ($0, $1, $2) {
		if ($1) uri[o.q.name][$1] = $2;
	});

	return uri;
};

parseUri.options = {
	strictMode: false,
	key: ["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","anchor"],
	q:   {
		name:   "queryKey",
		parser: /(?:^|&)([^&=]*)=?([^&]*)/g
	},
	parser: {
		strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
		loose:  /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
	}
};

// Global running/locked tasks counter
var Registry = function(){

	function add(key, value){
		console.debug('In --> Registry.add() | key : ' + key);
		localStorage[key] = JSON.stringify(value);
	}
	
	function get(key){
		console.debug('In --> Registry.get() | key : ' + key);
		if(!localStorage[key]){
			var obj = { 
				running 	: [],
				locked 		: [],
				rawtotal 	: 0,
				last_check 	: false,
				reset : function(){
					var  self = mCounter;
					self.running = [];
					self.locked = [];
					self.rawtotal = 0;
					self.last_check = false;
				}
			};
			localStorage[key] = JSON.stringify(obj);
			console.debug('In --> Registry.get() | New key --> ' + key + ', localStorage[key] : ' + JSON.stringify(obj));
			return obj;
		} else {
			console.debug('In --> Registry.get() | Existing key --> ' + key + ', localStorage[key] : ' + localStorage[key]);
			return JSON.parse(localStorage[key]);
		}		
	}
	
	function removeItem(key){
		console.debug('In --> Registry.removeItem() | key : ' + key);
		if(localStorage[key]){
			localStorage.removeItem(key);
		}
	}
	
	function init(){
		console.log('Executing --> Registry.init()');
	}
	  
	return {
		init : init,	
		add  : add,
		get  : get,
		removeItem  : removeItem
	};
}();

Registry.init();
//*/

//*
var mCounter = { 
	running 	: [],
	locked 		: [],
	rawtotal 	: 0,
	last_check 	: false,
	reset : function(){
		var  self = mCounter;
		self.running = [];
		self.locked = [];
		self.rawtotal = 0;
		self.last_check = false;
	}
};
//*/
					


function handleTasklistResult(tab, data){
	console.info("Executing --> handleTasklistResult");
	var nbTasks = data.length;
	var uriObj = parseUri(tab.url);

	mCounter.reset();
	mCounter.rawtotal = data.length || 0;
	
	var content = '<div style="background-color:#B1C903;color:#fff;font-weight:bold;padding:10px;text-align:center"><p style="font-size:1.5em;color:#000">'
					+ uriObj.host 
					+ '<p><br/>' 
					+ '<div>[ TASKS ] | Running OK : <span id="cmonitor-status-running" style="color:green">0</span>&nbsp;, &nbsp;Locked : <span id="cmonitor-status-locked" style="color:red">0</span></div>' 
					+ '</div>';	
	var updatercode = 'var holder = jQuery("body #cmonitor-chrome"); if(holder.length <= 0){ jQuery("body").prepend(\'<div id="cmonitor-chrome"></div>\'); holder = jQuery("body #cmonitor-chrome"); holder.prepend(\''+ content +'\'); }';

	chrome.tabs.executeScript({
		code: updatercode
	});
	
	
	var startingpoint	= $.Deferred(), 
		taskList = [];	

	startingpoint.resolve();
		
	var taskurls = data || [];
	if(taskurls.length){
		$.each(taskurls,function(index,taskdata) {
			var uriObj = parseUri(decodeURIComponent(taskdata));
			taskname = uriObj.queryKey.taskname || null;									
			console.log("{"+taskname + "} has index {" + taskList.indexOf(taskname)+ "} in {taskList} which has {"+taskList.length+"} entries");
			
			if(taskList.indexOf(taskname) === -1){	
				taskList.push(taskname);									
				startingpoint=startingpoint.pipe( function() {
						return fireRequestForTab(tab, taskdata, taskList);
				});
			} else {
				console.log("Request already made for {"+ taskname + "}");
			}
		});
	}	
}

function updateTasksCount(tab, databag){
	var mCounter = databag;
	
	console.debug("In --> updateTasksCount | Running : " + mCounter.running.length+", Locked : " + mCounter.locked.length);
	
	var selectorRunning = '#cmonitor-status-running';
	var selectorLocked = '#cmonitor-status-locked';
	var updaterunning = '$("'+selectorRunning+'").html("'+ mCounter.running.length +'")';
	var updatelocked = '$("'+selectorLocked+'").html("'+ mCounter.locked.length +'")';
	var updateLockedList = '';
	if(mCounter.locked.length){
		var lockedTasks = mCounter.locked.join(", ");
		updateLockedList = '$("'+selectorLocked+'").parent("div").attr("title", "'+ lockedTasks +'")';
		updatelocked += ';'+updateLockedList;
	}
	var updatercode = updaterunning+';'+updatelocked;

	chrome.tabs.executeScript({
		code: updatercode
	});
}

function fireRequestForTab(tab, taskdata, taskList) {
	console.info("Executing --> fireRequestForTab");
	var uriObj, taskname, target, taskstatusUrl;
	uriObj = parseUri(decodeURIComponent(taskdata));
	taskname = uriObj.queryKey.taskname || null;
	taskstatusUrl = taskdata;

	return $.ajax({
			type: "GET",
			url: taskstatusUrl,
			success: function(response, status){
					if(response){
						if("OK" === response.toUpperCase()){
							mCounter.running.push(taskname);
						}
						if("KO" === response.toUpperCase()){
							mCounter.locked.push(taskname);
						}							
					}
					console.debug("In fireRequestForTab | Running : " + mCounter.running.length + ", locked : " + mCounter.locked.length);							
					updateTasksCount(tab, mCounter);
					var numTask = taskList.indexOf(taskname) + 1;
					var countTasks = taskList.length;
					var lastTask = countTasks > 0 &&  countTasks === numTask;
					console.log('numTask : ' + numTask + ', countTasks : ' + countTasks +', lastTask : '+ lastTask);					
					if(lastTask){
						var cacheKey = uriObj['host'].replace(/\./g, '_');
						mCounter.checked = true;
						mCounter.last_check = new Date();
						Registry.add(cacheKey, mCounter);
					}
			}
	});
}

				
function handleCheckRequest(tab) {
	console.info("Executing --> handleCheckRequest");
	
	// 0) Init work variables
	var uriObj = parseUri(decodeURIComponent(tab.url));
	console.debug("In --> handleCheckRequest | host : " + uriObj['host']);
	
	// 1) Check validity of the current protocol/scheme
	var validSchemes = ['http', 'https'];
	if(validSchemes.indexOf(uriObj.protocol) === -1){
		var errorMessage = chrome.i18n.getMessage('errorInvalidProtocol');
		alert(errorMessage);
		return;
	}

	var cacheKey = uriObj['host'].replace(/\./g, '_');	
	console.debug("In --> handleCheckRequest | cacheKey : " + cacheKey);

	var updatercode = 'var holder = jQuery("body #cmonitor-chrome"); if(holder.length <= 0){ jQuery("body").prepend(\'<div id="cmonitor-chrome"></div>\'); '
					+ 'holder = jQuery("body #cmonitor-chrome"); holder.prepend(\'' 
					+ '<div style="background-color:#B1C903;color:#fff;font-weight:bold;padding:10px;text-align:center">'
					+ '<p style="font-size:1.5em;color:#000">' + uriObj.host + '<p><br/>' 
					+ '<div>[ TASKS ] | Running OK : <span id="cmonitor-status-running" style="color:green">0</span>&nbsp;, '
					+ '&nbsp;Locked : <span id="cmonitor-status-locked" style="color:red">0</span></div>' 
					+ '</div>\'); }';	
	
	chrome.tabs.executeScript({
		code: updatercode
	});


	// Check already done
	
	var mCounter = Registry.get(cacheKey);
	if(mCounter && mCounter.checked){
		var message = '{'+ uriObj.host + '} --> ' + chrome.i18n.getMessage('domainAlreadyChecked') + ' ( ' + mCounter.last_check + ' )';
		console.log(message);
		updateTasksCount(tab, mCounter);		
		return;
	} 
	
	var tasklistUrl = uriObj.protocol + "://" + uriObj.host + "/index.php?module=task&action=ListMonitoringURLS";
	console.debug("In --> handleCheckRequest | tasklistUrl : "+tasklistUrl);
	
	$.ajax({
		url: tasklistUrl,
		dataType : 'json',
		success: function(data) {
			handleTasklistResult(tab, data);
		},
		error: function(error) {
			var errorMessage = chrome.i18n.getMessage('errorMonitoringUnavailable');
			alert(errorMessage);
		}
	});
}

chrome.browserAction.onClicked.addListener(handleCheckRequest);